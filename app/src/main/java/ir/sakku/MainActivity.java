package ir.sakku;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_view;
    Button first, second, third;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        txt_view.setText(R.string.txt_result);

    }

    void bind() {
        txt_view = findViewById(R.id.txt_view);
        findViewById(R.id.first).setOnClickListener(this);
        findViewById(R.id.second).setOnClickListener(this);
        findViewById(R.id.third).setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.first){
            txt_view.setText(R.string.txt_winner);
            txt_view.setTextColor(Color.parseColor("#CCFF4081"));
            Toast.makeText(MainActivity.this,"Congratulation0_0", Toast.LENGTH_LONG).show();
        }else if(v.getId()==R.id.second){
            txt_view.setText(R.string.txt_second);
            txt_view.setTextColor(Color.parseColor("#FF3F51B5"));
        }else{
           txt_view.setText(R.string.txt_third);
           txt_view.setTextColor(Color.parseColor("#CFC0DE2C"));}
    }

}
